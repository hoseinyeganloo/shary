﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.Download
{
    public class DownloadSession
    {
        public virtual Guid Id { get; set; }

        public virtual long LinkId
        {
            get
            {
                if (Link != null)
                    return Link.Id;
                else
                    return -1;
            }
        }

        public virtual Link Link { get; set; }

        public virtual int UserId
        {
            get
            {
                if (User != null)
                    return User.Id;
                else
                    return -1;
            }
        }

        public virtual CMS.User User { get; set; }

        public virtual DateTime CreateDate { get; set; }

        public virtual DateTime ExpireOn { get; set; }

        public virtual byte Status { get; set; }
    }
}
