﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.Download.Maps
{
    class DownloadSessionMap:ClassMap<DownloadSession>
    {
        public static string schema = "[dbo]";
        public static string table = "[DownloadSession]";

        public DownloadSessionMap()
        {
            Id(x => x.Id).Column("[Id]").GeneratedBy.Guid();

            Map(x => x.CreateDate).Column("[CreateDate]").Not.Nullable();
            Map(x => x.ExpireOn).Column("[ExpireOn]").Not.Nullable();
            Map(x => x.Status).Column("[Status]").Not.Nullable();

            References<CMS.User>(x => x.User).Column("[UserId]").Not.Nullable().Cascade.None();
            References<Link>(x => x.Link).Column("[LinkId]").Not.Nullable().Cascade.None();
        }

    }
}
