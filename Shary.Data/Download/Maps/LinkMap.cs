﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.Download.Maps
{
    class LinkMap : ClassMap<Link>
    {
        public static string schema = "[dbo]";
        public static string table = "[Link]";

        public LinkMap()
        {
            Id(x => x.Id).Column("[Id]").GeneratedBy.Identity();

            Map(x => x.ServerSideAddress).Column("[ServerSideAddress]").Not.Nullable();
            Map(x => x.Size).Column("[Size]").Not.Nullable();
            Map(x => x.Name).Column("[Name]").Not.Nullable();
            Map(x => x.Type).Column("[Type]").Not.Nullable();
            Map(x => x.Status).Column("[Status]").Not.Nullable();

            References<CMS.Post>(x => x.Post).Column("[PostId]").LazyLoad();
            HasMany<DownloadSession>(x => x.Sessions).Inverse().KeyColumn("[LinkId]").LazyLoad().Cascade.SaveUpdate();
        }
    }
}
