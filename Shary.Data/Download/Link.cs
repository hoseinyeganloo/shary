﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.Download
{
    public class Link
    {
        public Link() { Sessions = new List<DownloadSession>(); }

        public virtual long Id { get; set; }

        public virtual long PostId
        {
            get
            {
                if (Post != null)
                    return Post.Id;
                else
                    return -1;
            }
        }

        public virtual CMS.Post Post { get; set; }

        public virtual int Size { get; set; }

        public virtual string Name { get; set; }

        public virtual string Type { get; set; }

        public virtual string ServerSideAddress { get; set; }

        public virtual byte Status { get; set; }

        public virtual IList<DownloadSession> Sessions { get; set; }
    }
}
