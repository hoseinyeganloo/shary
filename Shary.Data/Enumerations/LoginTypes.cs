﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.Enumerations
{
    public enum LoginTypes : byte
    {
        None = 0,
        Normal = 1,
        SignUp = 2,
        Logout = 3
    }
}
