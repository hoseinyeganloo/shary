﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.Enumerations
{
    public enum PostStatus : byte
    {
        None = 0, Approved = 1, Active = 2
    }

    public enum LinkStatus : byte
    {
        None = 0, Approved = 1, Active = 2
    }

    public enum DownloadSessionStatus : byte
    {
        None = 0, Active = 1
    }

    public enum UserStatus : byte
    {
        None = 0, Active = 1, Protected = 2
    }
}
