﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;


namespace Shary.Data.CMS
{
    public class User
    {
        public static string Anonymous_mail { get { return "Anonymous@Shary.com"; } }

        public User()
        {
            Posts = new List<Post>();
            DownloadSessions = new List<Download.DownloadSession>();
            Roles = new List<UserInRole>();
        }

        public virtual int Id { get; set; }

        public virtual IList<Post> Posts { get; set; }

        public virtual IList<Download.DownloadSession> DownloadSessions { get; set; }

        public virtual IList<UserInRole> Roles { get; set; }

        [Required(ErrorMessage = "وارد کردن ایمیل اجباری است.")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "آدرس ایمیل نمی تواند بیش از 100 کاراکتر و یا کمتر از 5 کاراکتر باشد.")]
        public virtual string Mail { get; set; }

        [StringLength(50, MinimumLength = 6, ErrorMessage = "کلمه ی عبور باید بیش از 5 کاراکتر و کمتر از 51 کاراکتر باشد.")]
        [Required(ErrorMessage = "وارد کردن کلمه ی عبور اجباری است.")]
        public virtual string Password { get; set; }

        [StringLength(50, ErrorMessage = "نام مستعار نباید بیش از 50 کاراکتر باشد.")]
        public virtual string NikName { get; set; }

        public virtual double StorageCapacity { get; set; }

        public virtual short SpeedLimit { get; set; }

        public virtual byte Status { get; set; }

        public virtual bool ISAnonymous
        {
            get
            {
                return Mail == Anonymous_mail;
            }
        }
    }
}
