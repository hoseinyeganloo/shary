﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.CMS
{
    public class Pics
    {
        public virtual Guid Id { get; set; }

        public virtual Guid LinkId { get; set; }

        public virtual Download.Link Link { get; set; }
        
    }
}
