﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.CMS
{
    public class PostProperty
    {
        public PostProperty()
        {
            CharValue = "";
        }

        public virtual long Id { get; set; }

        public virtual Post Post { get; set; }

        public virtual Property Property { get; set; }

        public virtual string CharValue { get; set; }

        public virtual double FloatValue { get; set; }
    }
}
