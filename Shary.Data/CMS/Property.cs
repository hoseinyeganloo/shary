﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.CMS
{
    public class Property
    {
        public Property()
        {
            PostProperties = new List<PostProperty>();
        }

        public virtual byte Id { get; set; }

        public virtual string Title { get; set; }

        public virtual IList<PostProperty> PostProperties { get; set; }
    }
}
