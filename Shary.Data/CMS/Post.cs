﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.CMS
{
    public class Post
    {

        public Post()
        {
            Properties = new List<PostProperty>();
            Links = new List<Download.Link>();
        }

        public virtual long Id { get; set; }

        public virtual string Title { get; set; }

        public virtual string Brif { get; set; }

        public virtual int OwnerId
        {
            get
            {
                if (Owner != null)
                    return Owner.Id;
                else
                    return -1;
            }
        }

        public virtual User Owner { get; set; }

        public virtual DateTime CreateDate { get; set; }

        public virtual DateTime ModifyDate { get; set; }

        public virtual int ModifierId { get; set; }

        public virtual byte Status { get; set; }

        public virtual string Tags
        {
            get
            {
                var tmp = Properties.Where(q => q.Property.Id == 3);
                var res = "";
                foreach (var i in tmp)
                    res += i.CharValue + ",";
                res = res.TrimEnd(',');
                return res;
            }
        }

        public virtual string Context { get; set; }

        public virtual IList<PostProperty> Properties { get; set; }
        public virtual IList<Download.Link> Links { get; set; }
    }
}
