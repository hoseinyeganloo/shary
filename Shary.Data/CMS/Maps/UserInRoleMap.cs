﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.CMS.Maps
{
    class UserInRoleMap:ClassMap<UserInRole>
    {
        public static string schema = "[dbo]";
        public static string table = "[UserInRole]";

        public UserInRoleMap()
        {
            Id(x => x.Id).Column("[Id]").GeneratedBy.Identity();

            References<Role>(x => x.Role).Column("[RoleId]").Not.Nullable().Cascade.None();
            References<User>(x => x.User).Column("[UserId]").Not.Nullable().Cascade.None();
        }
    }
}
