﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.CMS.Maps
{
    class PostPropertyMap : ClassMap<PostProperty>
    {
        public static string schema = "[dbo]";
        public static string table = "[PostPeroperty]";

        public PostPropertyMap()
        {
            Id(x => x.Id).Column("[Id]").GeneratedBy.Identity();

            Map(x => x.CharValue).Column("[CharValue]").Not.Nullable();
            Map(x => x.FloatValue).Column("[FloatValue]").Not.Nullable();

            References<Post>(x => x.Post).Column("[PostId]").Not.Nullable().Cascade.None().LazyLoad();
            References<Property>(x => x.Property).Column("[PropertyId]").Not.Nullable().Cascade.SaveUpdate();
        }
    }
}
