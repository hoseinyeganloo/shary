﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.CMS.Maps
{
    class PicMap:ClassMap<Pics>
    {
        public static string schema = "[dbo]";
        public static string table = "[Pics]";

        public PicMap()
        {
            Id(x => x.Id).Column("[Id]").GeneratedBy.Guid();

            References<Download.Link>(x => x.Link).Column("[LinkId]").Not.Nullable().Cascade.None();
        }
    }
}
