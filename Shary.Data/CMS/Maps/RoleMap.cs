﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.CMS.Maps
{
    class RoleMap:ClassMap<Role>
    {
        public static string schema = "[dbo]";
        public static string table = "[Role]";

        public RoleMap()
        {
            Id(x => x.Id).Column("[Id]").GeneratedBy.Identity();

            Map(x => x.Title).Column("[Title]").Not.Nullable();
            Map(x => x.Name).Column("[Name]").Not.Nullable();
            HasMany<UserInRole>(x => x.UsersInThisRole).Inverse().KeyColumn("[RoleId]").Cascade.None().LazyLoad();
        }
    }
}
