﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.CMS.Maps
{
    public class UserMap : ClassMap<User>
    {
        public static string schema = "[dbo]";
        public static string table = "[User]";

        public UserMap()
        {
            Table(table);
            Schema(schema);

            Id(x => x.Id).Column("[Id]").GeneratedBy.Identity();

            Map(x => x.Password).Column("[Password]").Not.Nullable().LazyLoad();
            
            HasMany<Post>(x => x.Posts).Inverse().KeyColumn("[OwnerId]").LazyLoad().Cascade.SaveUpdate();
            HasMany<Download.DownloadSession>(x => x.DownloadSessions).Inverse().KeyColumn("[UserId]").LazyLoad().Cascade.SaveUpdate();
            HasMany<UserInRole>(x => x.Roles).Inverse().KeyColumn("UserId").Cascade.SaveUpdate();

            Map(x => x.Mail).Column("[Mail]").Not.Nullable().Unique();
            Map(x => x.NikName).Column("[NikName]").Not.Nullable();
            Map(x => x.StorageCapacity).Column("[StorageCapacity]").Not.Nullable();
            Map(x => x.SpeedLimit).Column("[SpeedLimit]").Not.Nullable();
            Map(x => x.Status).Column("[Status]").Not.Nullable();

        }
    }
}
