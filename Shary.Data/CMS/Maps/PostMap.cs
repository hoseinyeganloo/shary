﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.CMS.Maps
{
    public class PostMap:ClassMap<Post>
    {
        public static string schema = "[dbo]";
        public static string table = "[Post]";

        public PostMap()
        {
            Id(x => x.Id).Column("[Id]").GeneratedBy.Identity();

            Map(x => x.Title).Column("[Title]").Not.Nullable();
            Map(x => x.Brif).Column("[Brif]").Not.Nullable();
            References<User>(x => x.Owner).Column("[OwnerId]").Not.Nullable().Cascade.None().LazyLoad();
            Map(x => x.CreateDate).Column("[CreateDate]").Not.Nullable();
            Map(x => x.ModifierId).Column("[Modifier]").Not.Nullable();
            Map(x => x.ModifyDate).Column("[ModifyDate]").Not.Nullable();
            Map(x => x.Status).Column("[Status]").Not.Nullable();
            Map(x => x.Context).Column("[Context]").Not.Nullable().LazyLoad();

            HasMany<PostProperty>(x => x.Properties).Inverse().KeyColumn("[PostId]").Cascade.SaveUpdate();
            HasMany<Download.Link>(x => x.Links).Inverse().KeyColumn("[PostId]").Cascade.SaveUpdate();
        }
    }
}
