﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Shary.Data.CMS.Maps
{
    class PropertyMap:ClassMap<Property>
    {
        public static string schema = "[dbo]";
        public static string table = "[Property]";

        public PropertyMap()
        {
            Id(x => x.Id).Column("[Id]").GeneratedBy.Identity();

            Map(x => x.Title).Column("[Title]").Not.Nullable();

            HasMany<PostProperty>(x => x.PostProperties).Inverse().KeyColumn("[PropertyId]").Cascade.SaveUpdate();
        }
    }
}
