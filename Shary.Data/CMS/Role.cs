﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.CMS
{
    public class Role
    {
        public Role() { UsersInThisRole = new List<UserInRole>(); }

        public virtual byte Id { get; set; }

        public virtual string Title { get; set; }

        public virtual string Name { get; set; }

        public virtual IList<UserInRole> UsersInThisRole { get; set; }
    }
}
