﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.CMS
{
    public class UserInRole
    {
        public virtual int Id { get; set; }

        public virtual byte RoleId
        {
            get
            {
                if (Role != null)
                    return Role.Id;
                else
                    return 0;
            }
        }

        public virtual int UserId
        {
            get
            {
                if (User != null)
                    return User.Id;
                else
                    return -1;
            }
        }

        public virtual Role Role { get; set; }

        public virtual User User { get; set; }
    }
}
