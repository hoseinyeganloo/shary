﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shary.Data.BusinesModels
{
    public class KeyValue<TKey, TVal>
    {
        public TKey Key { get; set; }
        public TVal Value { get; set; }
    }
}
