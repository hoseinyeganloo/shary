﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Shary.Data.Download;

namespace Shary.Models
{
    public static class FileManager
    {
        public static Link GetFileInfo(long id)
        {
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                return dc.Get<Link>(id);
            }
        }
    }
}