﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Shary.Data.Download;
using Shary.Data.Enumerations;
using HYUtils.OpenSource.Models;

namespace Shary.Models
{
    public static class DownloadManager
    {
        public const int expDate = 7;

        public static DownloadSession GetActiveSession(long? FileId)
        {
            var usr = UserManager.CurrentUser;
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var ss = dc.QueryOver<DownloadSession>().Where(q => q.Link.Id == FileId && q.User.Id == usr.Id).List<DownloadSession>();
                using (var tt = dc.BeginTransaction())
                {
                    foreach (var s in ss)
                    {
                        if (!usr.ISAnonymous)
                            s.ExpireOn = DateTime.UtcNow.AddDays(expDate);
                        if (s.ExpireOn < s.CreateDate.AddDays(expDate - 1))
                            s.Status = (byte)(s.Status & ~((byte)DownloadSessionStatus.Active));
                    }
                    tt.Commit();
                }
                if (usr.ISAnonymous)
                    return null;
                else
                    return ss.SingleOrDefault(q => (q.Status & (byte)DownloadSessionStatus.Active) != 0);
            }

        }

        public static AjaxProcessResult CreateSession(long FileId)
        {
            var res = new AjaxProcessResult();
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var usr = dc.Get<Data.CMS.User>(UserManager.CurrentUser.Id);
                var lnk = dc.Get<Link>(FileId);
                if ((lnk.Status & (byte)LinkStatus.Active) == 0)
                {
                    res.Results.Add("CreateSession", new ResultContainer(false, "چنین فایلی وجود ندارد."));
                    return res;
                }
                var ds = new DownloadSession { Id = Guid.NewGuid(), CreateDate = DateTime.UtcNow, ExpireOn = DateTime.UtcNow.AddDays(expDate), Status = (byte)DownloadSessionStatus.Active };
                using (var tr = dc.BeginTransaction())
                {
                    ds.User = usr;
                    ds.Link = lnk;
                    dc.Save(ds);
                    tr.Commit();
                    res.Results.Add("CreateSession", new ResultContainer(true, res: ds));
                }
                return res;
            }
        }

        public static DownloadSession GetSession(Guid Id, bool loadLink = false)
        {
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var ss = dc.QueryOver<DownloadSession>().Where(q => q.Id == Id).SingleOrDefault();
                if (loadLink)
                    ss.Link.ServerSideAddress.Any();
                return ss;
            }
        }
    }
}