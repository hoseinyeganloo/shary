﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using HYUtils.OpenSource.Models;
using HYUtils.OpenSource.Extentions;
using Shary.Data.CMS;
using Shary.Data.Download;
using Shary.Data.Enumerations;
using Shary.Data.BusinesModels;

using NHibernate;
using NHibernate.Linq;

namespace Shary.Models
{
    public static class PostManager
    {
        public static string[] ExtensionDefiner = new string[] { "Audio:mp3,wma,wav,wave,oga,ogg,ra,ram,sng,mid", "Video:3g2,3gp,mp4,mp4v,mpe,mpeg,mpeg1,mpeg4,ogm,ogv,ogx,wmv,avi,flv,mov,swf,vob", "Image:bmp,dds,gif,jpg,png,psd,pspimage,tif,tiff,jpeg,psp,img,ai,eps,ps,svg,asy,3dm,3ds,max,blend,ma,mb,dwf,dat", "BackUp:bak,tmp,arc,bkp,", "Text:txt,doc,docx,csv,py,cs,vb" };

        public static AjaxProcessResult Create(Post post, IEnumerable<HttpPostedFileBase> files, IEnumerable<string> Tags)
        {
            var apr = new AjaxProcessResult();
            if (UserManager.CurrentUser.ISAnonymous)
                apr.Results.Add("Create", new ResultContainer(false, "برای ایجاد پست جدید باید ابتدا وارد سایت شوید."));
            else if (!UserManager.CurrentUser.Roles.Any(q => q.Role.Name == "PostManager"))
                apr.Results.Add("Create", new ResultContainer(false, "نام کاربری شما اجازه ی ثبت پست جدید ندارد."));
            else if (!files.Any(q => q != null))
                apr.Results.Add("Create", new ResultContainer(false, "هیج فایلی ارسال نشده است"));
            else
                using (var da = DataProvider.DataContext.OpenSession())
                {
                    if (apr.Success)
                    {
                        post.Brif = string.IsNullOrEmpty(post.Brif) ? post.Context.Substring(0, post.Context.Length > 200 ? 200 : post.Context.Length) : post.Brif;
                        post.CreateDate = DateTime.Now;
                        post.ModifierId = UserManager.CurrentUser.Id;
                        post.ModifyDate = post.CreateDate;
                        post.Owner = UserManager.CurrentUser;
                        post.Status = (byte)PostStatus.Approved | (byte)PostStatus.Active;
                        var prop = da.QueryOver<Property>().Where(q => q.Title == "Size" || q.Title == "Type" || q.Title == "Tag").List();
                        string directory = "/Files/" + UserManager.CurrentUser.Id + "/" + post.Title + "/";
                        string mapedDir = HttpContext.Current.Server.MapPath(directory);
                        foreach (var file in files.Where(q => q != null))
                        {
                            var name = Guid.NewGuid().ToString();
                            if (!System.IO.Directory.Exists(mapedDir))
                                System.IO.Directory.CreateDirectory(mapedDir);
                            if (System.IO.File.Exists(mapedDir + name))
                                System.IO.File.Delete(mapedDir + name);
                            file.SaveAs(mapedDir + name);
                            var nsp = file.FileName.Split('.');
                            var lnk = new Link { ServerSideAddress = directory + name, Size = file.ContentLength, Name = nsp[0], Type = nsp.Length > 1 ? nsp[1] : "", Status = ((byte)LinkStatus.Approved | (byte)LinkStatus.Active), Post = post };
                            post.Links.Add(lnk);
                            string type = "";
                            type = ExtensionDefiner.SingleOrDefault(q => q.Split(':')[1].Split(',').Contains(lnk.Type));
                            if (string.IsNullOrEmpty(type))
                                type = "NAN";
                            else
                                type = type.Split(':')[0];
                            post.Properties.Add(new PostProperty { Post = post, CharValue = type, Property = prop.Single(q => q.Title == "Type") });
                            post.Properties.Add(new PostProperty { Post = post, FloatValue = lnk.Size, Property = prop.Single(q => q.Title == "Size") });
                        }
                        foreach (var tg in Tags)
                        {
                            post.Properties.Add(new PostProperty { Post = post, CharValue = tg, Property = prop.Single(q => q.Title == "Tag") });
                        }
                        UserManager.CurrentUser.Posts.Add(post);
                        using (var tr = da.BeginTransaction())
                        {
                            da.Save(post);
                            tr.Commit();
                        }
                        apr.Results.Add("1", new ResultContainer(true, "اطلاعات با موفقیت در سیستم ثبت شد.", post.Id, messageType: (byte)AjaxResultMessageType.Alert));
                    }
                }
            return apr;
        }

        public static Post Get(long Id, bool loadContext, bool LoadLinks, bool loadProps)
        {
            Post ps = null;
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                ps = dc.Get<Post>(Id);
                if (loadContext)
                    ps.Context = ps.Context;
                if (LoadLinks)
                    ps.Links.Any();
                if (loadProps)
                    ps.Properties.Any();
                return ps;
            }
        }

        public static IList<Post> GetAllPosts(PostSearchModel sobj, bool loadProps)
        {
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var res = dc.Query<Post>();
                if (sobj.userId.HasValue)
                    res = res.Where(q => q.Owner.Id == sobj.userId);
                if (!string.IsNullOrEmpty(sobj.Title))
                    res = res.Where(q => q.Title.Contains(sobj.Title));

                if (!sobj.Status.HasValue && !sobj.ContainStatus.HasValue)
                    res = res.Where(q => (q.Status & (byte)PostStatus.Active) != 0);
                if (sobj.Status.HasValue)
                    res = res.Where(q => q.Status == sobj.Status);
                if (sobj.ContainStatus.HasValue)
                    res = res.Where(q => (q.Status & sobj.ContainStatus) == sobj.ContainStatus);
                if (sobj.property != null)
                    res = res.Where(q => q.Properties.Any(p => p.Property.Id == sobj.property.Key && p.CharValue == sobj.property.Value));
                if (!string.IsNullOrEmpty(sobj.Tag))
                    res = res.Where(q => q.Properties.Any(p => p.CharValue.Contains(sobj.Tag)));
                var ret = res.Paging(sobj.PageIndex, sobj.PageSize).ToList();
                if (loadProps)
                    ret = ret.Select(q => new Post { Properties = q.Properties.Select(p => p).ToList(), Brif = q.Brif, CreateDate = q.CreateDate, Id = q.Id, Status = q.Status, Title = q.Title }).ToList();
                return ret;
            }
        }

        public static Property GetProperties(string Name)
        {
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var tmp = dc.QueryOver<Property>().Where(q => q.Title == Name).SingleOrDefault();
                tmp.PostProperties.Any();
                return tmp;
            }
        }

        public static AjaxProcessResult DeletePost(long Id)
        {
            var res = new AjaxProcessResult();
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var cru = UserManager.CurrentUser;
                var post = dc.Get<Post>(Id);
                if (post.OwnerId != cru.Id && !cru.Roles.Any(q => q.Role.Name == "PostManager"))
                {
                    res.Results.Add("DeletePost", new ResultContainer(false, "شما اجازه ی ویرایش این پست را ندارید."));
                    return res;
                }
                using (var tr = dc.BeginTransaction())
                {
                    post.Status = (byte)(post.Status & ~(byte)PostStatus.Active);
                    post.Links.ForEach((l) => { l.Status = (byte)(l.Status & ~(byte)LinkStatus.Active); l.Sessions.ForEach((s) => s.Status = (byte)(s.Status & ~(byte)DownloadSessionStatus.Active)); });
                    tr.Commit();
                }
                res.Results.Add("DeletePost", new ResultContainer(true, "این پست پاک شد.", messageType: (byte)AjaxResultMessageType.Alert));
                res.Script.Add("location.reload();");
            }
            return res;
        }

    }

    public class PostSearchModel
    {
        public int? userId { get; set; }
        public string Title { get; set; }
        public KeyValue<byte, string> property { get; set; }
        public byte? Status { get; set; }
        public byte? ContainStatus { get; set; }
        public string Tag { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}

