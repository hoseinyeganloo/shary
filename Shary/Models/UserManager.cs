﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NHibernate.Linq;

using HYUtils.OpenSource.Models;
using HYUtils.OpenSource.Extentions;
using Shary.Data.CMS;
using Shary.Data.Enumerations;


namespace Shary.Models
{
    public static class UserManager
    {
        public static string UserSessionKey = "CurrentUser";

        public static string[] WeakPassword = new string[] { "Password", "P@ssword", "123456", "654321", "1234567", "7654321", "12345678", "87654321", "123456789", "987654321" };

        public static User CurrentUser
        {
            get
            {
                var ct = HttpContext.Current;
                using (var dc = DataProvider.DataContext.OpenSession())
                {
                    if (ct.Session == null || ct.Session[UserSessionKey] == null)
                    {
                        var anonymous = dc.QueryOver<User>().Where(q => q.Mail == User.Anonymous_mail).SingleOrDefault();
                        if (anonymous == null)
                        {
                            anonymous = new User() { Mail = User.Anonymous_mail, NikName = "کاربر ناشناس", Password = "123456", SpeedLimit = 10, Status = 1, StorageCapacity = 0 };
                            var Admin = new User() { Mail = "admin", NikName = "مدیر سایت", Password = "admin", SpeedLimit = -1, Status = 3, StorageCapacity = 0 };
                            string[] adroles = new string[] { "PostManager", "Admin" };
                            var rls = dc.Query<Role>().Where(q => adroles.Contains(q.Name));
                            foreach (var i in rls)
                                Admin.Roles.Add(new UserInRole { User = Admin, Role = i });
                            dc.Save(anonymous);
                            dc.Save(Admin);
                        }
                        else
                            anonymous = anonymous.ConvertToViewModel();
                        ct.Session.Add(UserSessionKey, anonymous);
                        return anonymous;
                    }
                    else
                    {
                        return (User)ct.Session[UserSessionKey];
                    }
                }
            }
        }

        public static IList<User> GetAllUsers(UserSearchModel sobj)
        {
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var res = dc.Query<User>().Select(q => q.ConvertToViewModel());
                return res.Paging(sobj.PageIndex, sobj.PageSize).ToList();
            }
        }

        public static AjaxProcessResult Login(string Mail, string Pass)
        {
            var res = new AjaxProcessResult();
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                User user = dc.QueryOver<User>().Where(q => q.Mail == Mail).SingleOrDefault();
                if (user == null || user.Password != Pass)
                    res.Results.Add("Login", new ResultContainer(false, "تام کاربری و یا کلمه ی عبور اشتباه است."));
                else if ((user.Status & (byte)UserStatus.Active) == 0)
                    res.Results.Add("Login", new ResultContainer(false, "نام کاربری شما غیر فعال شده است.", messageType: 3));
                else if (res.Success)
                {
                    res.Results.Add("Login", new ResultContainer(true, messageType: (byte)AjaxResultMessageType.Alert | (byte)AjaxResultMessageType.Text) { Message = "خوش آمدید." });
                    res.Script.Add("location.reload();");
                    HttpContext.Current.Session[UserSessionKey] = user.ConvertToViewModel();
                    HttpContext.Current.Session.Timeout = 10080;
                }
            }
            return res;
        }

        public static AjaxProcessResult LogOut()
        {
            HttpContext.Current.Session[UserSessionKey] = null;
            HttpContext.Current.Session.Timeout = 20;
            var res = new AjaxProcessResult();
            res.Results.Add("LogOut", new ResultContainer(true, "خدا نگهدار", messageType: (byte)AjaxResultMessageType.Alert | (byte)AjaxResultMessageType.Text));
            res.Script.Add("location.reload();");
            return res;
        }

        public static AjaxProcessResult Create(User user)
        {
            AjaxProcessResult res = new AjaxProcessResult();
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var tmp = dc.QueryOver<User>().Where(q => q.Mail == user.Mail).SingleOrDefault();
                if (tmp == null)
                {
                    if (user.Mail.IndexOf("@") >= user.Mail.IndexOf(".") || user.Mail.IndexOf("@") < 1)
                        res.Results.Add("Mail", new ResultContainer(false, "لطفا یک ایمیل معتبر وارد کنید."));
                    if (user.Password.Length < 6 || WeakPassword.Any(q => q.ToLower() == user.Password.ToLower()))
                        res.Results.Add("Password", new ResultContainer(false, "کلمه ی عبور انتخاب شده بیش از حد ساده و غیر قابل اطمینان است."));
                }
                else
                {
                    res.Results.Add("Create", new ResultContainer(false, "این نام کاربری پیش از این در سیستم ثبت شده است."));
                }

                if (res.Success)
                {
                    user.SpeedLimit = 10;
                    user.Status = 1;
                    user.StorageCapacity = 1.2;
                    user.NikName = string.IsNullOrEmpty(user.NikName) ? user.Mail.Split('@')[0] : user.NikName;
                    dc.Save(user);
                    res.Results.Add("Create", new ResultContainer(true, "نام کاربری شما با موفقیت ثبت شد.", user, messageType: (byte)AjaxResultMessageType.Alert | (byte)AjaxResultMessageType.Text));
                }
            }
            res.Script = Login(user.Mail, user.Password).Script;
            return res;
        }

        public static AjaxProcessResult AddRols(int userId, byte[] roleId)
        {
            var res = new AjaxProcessResult();
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var user = dc.Get<User>(userId);
                var rls = roleId.Where(q => !user.Roles.Select(p => p.Id).Contains(q)).ToList();
                var roles = dc.Query<Role>().Where(q => rls.Contains(q.Id));
                using (var tr = dc.BeginTransaction())
                {
                    foreach (var rl in roles)
                    {
                        user.Roles.Add(new UserInRole { Role = rl, User = user });
                    }
                    tr.Commit();
                }
            }
            return res;
        }

        public static AjaxProcessResult DeActive(int id)
        {
            var res = new AjaxProcessResult();
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var user = dc.Get<User>(id);
                if ((user.Status & (byte)UserStatus.Protected) != 0)
                {
                    res.Results.Add("DeActive", new ResultContainer(false, "این کاربر حفاظت شده و امکان غیر فعال کردن آن وجود ندارد."));
                    return res;
                }
                using (var tr = dc.BeginTransaction())
                {
                    user.Status = (byte)(user.Status & ~(byte)UserStatus.Active);
                    tr.Commit();
                }
            }
            return res;
        }

        public static AjaxProcessResult Activate(int id)
        {
            var res = new AjaxProcessResult();
            using (var dc = DataProvider.DataContext.OpenSession())
            {
                var user = dc.Get<User>(id);
                using (var tr = dc.BeginTransaction())
                {
                    user.Status = (byte)(user.Status | (byte)UserStatus.Active);
                    tr.Commit();
                }
            }
            return res;
        }

        public static User ConvertToViewModel(this User user)
        {
            User res = new User { Id = user.Id, Mail = user.Mail, NikName = user.NikName, Roles = null, SpeedLimit = user.SpeedLimit, Status = user.Status, StorageCapacity = user.StorageCapacity };
            var rols = new List<UserInRole>();
            foreach (var i in user.Roles)
                rols.Add(new UserInRole { Id = i.Id, User = res, Role = new Role { Id = i.Role.Id, Name = i.Role.Name, Title = i.Role.Title } });
            res.Roles = rols;
            return res;
        }
    }

    public class UserSearchModel
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}