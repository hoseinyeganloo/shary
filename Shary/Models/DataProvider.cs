﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using NHibernate;
using FluentNHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace Shary.Models
{
    public static class DataProvider
    {
        private static ISessionFactory sessionFactory = null;
        private static ISession currentSession = null;

        public static ISessionFactory DataContext
        {
            get
            {
                if (sessionFactory == null)
                    sessionFactory = Fluently.Configure().Database(MsSqlConfiguration.MsSql2012.ConnectionString(
                        s => s.Server("ROSHD").Database("Shary").Username("dv").Password("123456")
                        )).Mappings(m => m.FluentMappings.AddFromAssemblyOf<Shary.Data.CMS.Post>())
                        .BuildSessionFactory();
                return sessionFactory;
            }
        }
    }
}