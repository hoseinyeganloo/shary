﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;

using HYUtils.OpenSource.Models;
using Shary.Models;
using Shary.Data.CMS;
using Shary.Data.Enumerations;

using HYUtils.OpenSource.Extentions;

namespace Shary.Controllers
{
    public class HomeController : Controller
    {
        static Random rnd = new Random();

        // GET: /Home/
        public ActionResult Index()
        {
            return View(PostManager.GetAllPosts(new PostSearchModel { PageIndex = 0, PageSize = 10 }, true));
        }

        #region Login

        public FileContentResult captchaImage()
        {
            string arr = "!@#$QW1E2R3T4Y5U6I7P8A9SDFGHJKLZXCVBNM";
            string cpt = "";
            for (int i = 0; i < 6; i++)
            {
                cpt += arr[(int)(rnd.NextDouble() * arr.Length - 1)];
            }
            if (!string.IsNullOrEmpty((string)Session["captcha"]))
                Session["captcha"] = cpt;
            else
                Session.Add("captcha", cpt);
            return new FileContentResult(HYUtils.OpenSource.Graphic.Captcha.GenerateImage(150, 50, cpt), "Image/gif");
        }

        public ActionResult captcha()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View(UserManager.CurrentUser);
        }

        [HttpPost]
        public ActionResult DoLogin(User user, string RePass, LoginTypes login_type, string captcha)
        {
            var bpr = new AjaxProcessResult();
            if ((string.IsNullOrEmpty(captcha) || captcha.ToUpper() != Session["captcha"].ToString()) && login_type != LoginTypes.Logout)
                bpr.Results.Add("DoLogin", new ResultContainer(false, "لطفا عبارت داخل تصویر را صحیح وارد کنید.", messageType: (byte)AjaxResultMessageType.Alert));
            if (!bpr.Success)
                return View("AjaxProcess", bpr);
            switch (login_type)
            {
                case LoginTypes.Normal:
                    return View("AjaxProcess", UserManager.Login(user.Mail, user.Password));
                case LoginTypes.SignUp:
                    return View("AjaxProcess", UserManager.Create(user));
                case LoginTypes.Logout:
                    return View("AjaxProcess", UserManager.LogOut());
                default:
                    return null;
            }
        }

        #endregion

        #region Access Menu

        public ActionResult Menu()
        {
            return View();
        }

        #endregion

        #region Search

        public ActionResult Search(PostSearchModel sobj, FormCollection frm)
        {
            sobj.property = new Data.BusinesModels.KeyValue<byte, string> { Key = PostManager.GetProperties("Type").Id, Value = frm["Type"] };
            var res = PostManager.GetAllPosts(sobj, true);
            return View("~/Areas/Post/Views/Post/PostIx.cshtml", res);
        }

        #endregion
    }
}
