﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Shary.Models;

namespace Shary.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/Admin/

        public ActionResult Users()
        {
            if (!UserManager.CurrentUser.Roles.Any(q => q.Role.Name == "Admin"))
                return RedirectToAction("Index", "Home", new { area = "" });
            return View();
        }

        public ActionResult UsersIx(UserSearchModel sobj)
        {
            if (!UserManager.CurrentUser.Roles.Any(q => q.Role.Name == "Admin"))
                return RedirectToAction("Index", "Home", new { area = "" });
            return View(UserManager.GetAllUsers(sobj));
        }

        public ActionResult DeactiveUser(int id)
        {
            var res = UserManager.DeActive(id);
            if (!res.Success)
                return View("AjaxResult", res);
            return View("UsersIx", UserManager.GetAllUsers(new UserSearchModel()));
        }

        public ActionResult ActivateUser(int id)
        {
            var res = UserManager.Activate(id);
            if (!res.Success)
                return View("AjaxResult", res);
            return View("UsersIx", UserManager.GetAllUsers(new UserSearchModel()));
        }

    }
}
