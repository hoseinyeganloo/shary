﻿using System.Web.Mvc;

namespace Shary.Areas.Download
{
    public class DownloadAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Download";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Download_default",
                "Download/{controller}/{action}/{id}",
                new { action = "File", id = UrlParameter.Optional }
            );
        }
    }
}
