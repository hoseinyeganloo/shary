﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shary.Areas.Download.Models
{
    public class DownloadWaiteTime
    {
        public DateTime CreateTime { get; set; }
        public int waite { get; set; }
        public Guid DownloadSessionId { get; set; }
        public long FileId { get; set; }
    }
}