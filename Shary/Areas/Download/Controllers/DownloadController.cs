﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Threading;

using Shary.Models;
using System.Net;
using HYUtils.OpenSource.Models;

namespace Shary.Areas.Download.Controllers
{
    public class DownloadController : Controller
    {
        //
        // GET: /Download/Download/

        private bool WaitTimePassed(Guid SessionId)
        {
            Models.DownloadWaiteTime dwt;
            return WaitTimePassed(SessionId, out dwt);
        }
        private bool WaitTimePassed(Guid SessionId, out Models.DownloadWaiteTime dwt)
        {
            dwt = (Models.DownloadWaiteTime)Session["DownloadWaite_" + SessionId];
            var ss = DownloadManager.GetSession(SessionId);
            if (dwt == null)
            {
                if (ss == null)
                {
                    dwt = new Models.DownloadWaiteTime { FileId = -1 };
                    return false;
                }
                else
                {
                    dwt = new Models.DownloadWaiteTime { FileId = ss.LinkId };
                    if ((ss.Status & (byte)Data.Enumerations.DownloadSessionStatus.Active) == 0)
                        return false;
                    else
                        return true;
                }
            }
            if (DateTime.UtcNow.Subtract(dwt.CreateTime.AddSeconds(dwt.waite)).Seconds < 0)
            {
                dwt.CreateTime = DateTime.UtcNow;
                return false;
            }
            return true;
        }

        public ActionResult File(long id)
        {
            if (id < 1)
            {
                var res = new AjaxProcessResult();
                res.Results.Add("File", new ResultContainer(false, "این فایل پیدا نشد."));
                return View("AjaxProcess", res);
            }
            var session = DownloadManager.GetActiveSession(id);
            if (session != null && session.Id != Guid.Empty)
                return RedirectToAction("Download", new { id = session.Id });
            else
            {
                int waitTime = 20;
                var cs = DownloadManager.CreateSession(id);
                if (!cs.Success)
                {
                    ViewBag.AjaxResult = cs;
                    return View("~/Areas/Post/Views/Post/Post.cshtml");
                }
                var sid = (Data.Download.DownloadSession)cs.Results.Single().Value.Result;
                if (!UserManager.CurrentUser.ISAnonymous)
                    return RedirectToAction("Download", new { id = sid.Id });
                var wait = new Models.DownloadWaiteTime { CreateTime = DateTime.UtcNow, DownloadSessionId = sid.Id, FileId = id, waite = waitTime };
                ViewBag.wait = wait;
                Session.Add("DownloadWaite_" + sid.Id, wait);
                return View(FileManager.GetFileInfo(sid.LinkId));
            }
        }

        public ActionResult DownloadLink(Guid id)
        {
            Models.DownloadWaiteTime dwt;
            if (!WaitTimePassed(id, out dwt))
                return View("Timer", dwt);
            return View(DownloadManager.GetSession(id, true));
        }

        public ActionResult Download(Guid id)
        {
            var usr = UserManager.CurrentUser;
            Models.DownloadWaiteTime dwt;
            if (!WaitTimePassed(id, out dwt))
                return RedirectToAction("File", new { id = dwt.FileId });

            //start Download checkings
            var ss = DownloadManager.GetSession(id, true);

            string fileAddress = Server.MapPath("/") + ss.Link.ServerSideAddress;
            if (!System.IO.File.Exists(fileAddress))
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return null;
            }
            using (var f = new FileStream(fileAddress, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                if (f.Length > Int32.MaxValue)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    Response.StatusDescription = "Huge file!";
                    return null;
                }
                string fname = ss.Link.Name + "." + ss.Link.Type;
                string lastUpdate = System.IO.File.GetLastWriteTimeUtc(fileAddress).ToString("r");
                string filenameUrlEncoded = HttpUtility.UrlEncode(fname, Encoding.UTF8);
                string etag = filenameUrlEncoded + lastUpdate;

                string ifrange = Request.Headers["If-Range"];
                if (!string.IsNullOrEmpty(ifrange) && ifrange != etag)
                {
                    Response.StatusCode = (int)HttpStatusCode.PreconditionFailed;
                    return null;
                }

                long startAt = 0;
                string range = Request.Headers["Range"];
                if (!string.IsNullOrEmpty(range))
                {
                    Response.StatusCode = 206;
                    startAt = long.Parse(range.Split("=-".ToCharArray())[1]);
                    if (startAt < 0 || startAt > f.Length)
                    {
                        Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        Response.StatusDescription = "Start index (" + startAt + ") out of file range!";
                        return null;
                    }
                    else
                        f.Seek(startAt, SeekOrigin.Begin);
                }
                Response.Clear();
                Response.Buffer = false;
                Response.AddHeader("Last-Modified", lastUpdate);
                Response.AddHeader("ETag", etag);
                Response.AddHeader("Accept-Ranges", "bytes");
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filenameUrlEncoded);
                Response.AddHeader("Content-Length", (f.Length - startAt).ToString());
                Response.AddHeader("Connection", "Keep-Alive");
                Response.AddHeader("Content-Range", string.Format(" bytes {0}-{1}/{2}", startAt, f.Length - 1, f.Length));
                Response.ContentEncoding = Encoding.UTF8;


                using (var rd = new BinaryReader(f))
                {
                    int packetSize = 15 * 1024;
                    if (usr.ISAnonymous)
                        packetSize = 5 * 1024;
                    int total = (int)Math.Ceiling((double)f.Length / packetSize);
                    for (int i = 0; Response.IsClientConnected && i < total; i++)
                    {
                        Response.BinaryWrite(rd.ReadBytes(packetSize));
                        Response.Flush();
                        Thread.Sleep(1000);
                    }
                }
            }

            return null;
        }

    }
}
