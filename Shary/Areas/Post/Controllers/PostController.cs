﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

using Shary.Models;
using Shary.Data.CMS;
using HYUtils.OpenSource.Models;

namespace Shary.Areas.Post.Controllers
{
    public class PostController : Controller
    {
        //
        // GET: /Post/Post/

        public ActionResult Post(long id)
        {
            var p = PostManager.Get(id, true, true, true);
            if ((p.Status & (byte)Data.Enumerations.PostStatus.Active) == 0)
            {
                var res = new AjaxProcessResult();
                res.Results.Add("Post", new ResultContainer(false, "فایل مورد نظر پیدا نشد.", messageType: 3));
                ViewBag.AjaxResult = res;
                return View();
            }
            return View(p);
        }

        public ActionResult Manage()
        {
            if (UserManager.CurrentUser.ISAnonymous)
                return RedirectToAction("Index", "Home", new { area = "" });
            ViewBag.manage = true;
            return View(new Shary.Data.CMS.Post());
        }

        [HttpPost]
        public ActionResult Manage(Shary.Data.CMS.Post post, string tag, IEnumerable<HttpPostedFileBase> files)
        {
            if (UserManager.CurrentUser.ISAnonymous)
                return RedirectToAction("Index", "Home", new { area = "" });
            var tg = tag.Split(',');
            AjaxProcessResult res = PostManager.Create(post, files, tg);
            if (res.Success)
            {
                res.Results.Add("Manage", new ResultContainer(true, "لطفا چند لحظه منتظر بمانید."));
                res.Script.Add(@"$(function(){
    $('input').attr('disabled','disabled');
    window.location = '" + Url.Action("Post", "Post", new { area = "Post", id = (long)res.Results["1"].Result }) + "';})");
            }
            ViewBag.AjaxResult = res;
            return View(post);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            return View("AjaxProcess", PostManager.DeletePost(id));
        }

        public ActionResult PostIx(Shary.Data.CMS.Post sobj, int PageSize, int PageIndex, bool? manage)
        {
            ViewBag.manage = manage;
            return View(PostManager.GetAllPosts(new PostSearchModel { userId = UserManager.CurrentUser.Id, ContainStatus = (byte)Data.Enumerations.PostStatus.Active }, true));
        }

    }
}
